var myFunction = function () {

    const options = document.getElementById("options");
    const button = document.getElementById("add_to_bag");

    if (window.pageYOffset >= options.offsetTop) {
        button.classList.add("navigation");
    } else {
        button.classList.remove("navigation");
    }
}

window.onscroll = myFunction;
